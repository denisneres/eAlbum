import { Injectable, PipeTransform, Pipe } from "@angular/core";
import { Photo } from "../photo/photo";

@Pipe({name:'filterByTextTyped'})
export class FilterByTextTyped implements PipeTransform {
   
   
    transform(photos: Photo[],textTyped: string) {
        console.log(textTyped);
        return photos.filter(p => p.description.toLowerCase().includes(textTyped.toLowerCase()));
    }


}