import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";

import { PhotoComponent } from "./photo/photo.component";
import { HttpClientModule } from "@angular/common/http";
import { PhotosListComponent } from './photos-list/photos-list.component';
import { PhotosComponent } from "./photos-list/photos/photos.component";
import { FormsModule } from "@angular/forms";
import { FilterByTextTyped } from "./photos-list/filter-by-text-typed.pipe";
import { LoadButtonComponent } from "./photos-list/load-button/load-button.component";
import { CardModule } from "../shared/components/card/card.module";
import { PhotosFormComponent } from "./photos-form/photos-form.component";


@NgModule({
    declarations: [ 
        PhotoComponent, 
        PhotosListComponent,
        PhotosComponent,
        LoadButtonComponent,
        PhotosFormComponent,
        FilterByTextTyped
        
    ],
    imports: [ 
        HttpClientModule, 
        CommonModule,
        FormsModule,
        CardModule
        
    ]
})
export class PhotosModule {}