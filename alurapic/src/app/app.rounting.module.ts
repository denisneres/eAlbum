import { NgModule } from "@angular/core";
import { Routes, RouterModule} from '@angular/router';
import { AppComponent } from "./app.component";
import { PhotosListComponent } from "./photos/photos-list/photos-list.component";
import { NotFoundComponent } from "./errors/not-found/not-found.component";
import { PhotoListResolver } from "./photos/photos-list/photo-list.resolver";
import { SigninComponent } from "./home/signin/signin.component";
import { AuthGuard } from "./core/auth.guard";
import { SignupComponent } from "./home/signup/signup.component";
import { PhotosFormComponent } from "./photos/photos-form/photos-form.component";

const routes: Routes = [
    {
        path:'',
        component:SigninComponent,
        canActivate: [AuthGuard]
    
    },
    {
        path:'signup',
        component:SignupComponent
    
    },
    {
        path:'photos/add',
        component:PhotosFormComponent
    
    },
    {
        path:'user/flavio',
        component:PhotosListComponent,
        resolve:{
            photos: PhotoListResolver
        }
    },
    {
        path:'**',
        component: NotFoundComponent
    }
];

@NgModule({
     imports: [
        RouterModule.forRoot(routes)
    ],
    exports: [
        RouterModule
    ]
})
export class AppRoutingModule {}