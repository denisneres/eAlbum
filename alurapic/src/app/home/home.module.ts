import { NgModule } from "@angular/core";
import { SigninComponent } from "./signin/signin.component";
import { CommonModule } from "@angular/common";
import { ReactiveFormsModule, FormsModule } from "@angular/forms";
import { SignupComponent } from "./signup/signup.component";
import { RouterModule } from "@angular/router";



@NgModule({
    declarations: [SigninComponent, SignupComponent],
    exports:[SigninComponent, SignupComponent],
    imports: [
        CommonModule,
        ReactiveFormsModule,
        FormsModule,
        RouterModule
    ]
})
export class HomeModule {}