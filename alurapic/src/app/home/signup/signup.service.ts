import { Injectable } from "@angular/core";
import { NewUser } from "./new-user";
import { HttpClient } from "@angular/common/http";

const API = "http://localhost:3000"

@Injectable({providedIn:'root'})
export class SignupService {

    constructor(private http: HttpClient) {
        
    }

    
    signup(newUser: NewUser) {
        console.log(newUser);
        return this.http.post(API + '/user/signup', newUser );
    }

}