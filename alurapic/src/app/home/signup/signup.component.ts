import { Component } from "@angular/core";
import { FormGroup, FormBuilder, Validators } from "@angular/forms";
import { UserService } from "src/app/user/user.service";
import { NewUser } from "./new-user";
import { SignupService } from "./signup.service";
import { Router } from "@angular/router";

@Component({
    templateUrl:"signup.component.html"
})
export class SignupComponent {

    signupForm: FormGroup;

    constructor(private formBuilder: FormBuilder, private signupService: SignupService, private router: Router){

    }

    ngOnInit() {
        this.signupForm = this.formBuilder.group({
            userName: ['', Validators.required],
            fullName:['', Validators.required],
            email: ['', 
                [
                    Validators.required,
                    Validators.email
                ]         
            ],
            password: ['', Validators.required]
        })
    }

    signup() {
        const newUser = this.signupForm.getRawValue() as NewUser;
        this.signupService
            .signup(newUser)
            .subscribe(
                () => this.router.navigate(['']),
                err => console.log(err));
    }

}