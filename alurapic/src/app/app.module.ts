import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppComponent } from './app.component';

import { PhotosModule } from './photos/photos.module';
import { AppRoutingModule } from './app.rounting.module';
import { ErrorsModule } from './errors/errors.module';
import { HomeModule } from './home/home.module';
import { HeaderComponent } from './header/header.component';
import { HTTP_INTERCEPTORS } from '@angular/common/http';
import { RequestInterceptor } from './request.interceptor';
import { FooterComponent } from './footer/footer.component';
import { PhotosFormComponent } from './photos/photos-form/photos-form.component';

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    FooterComponent
  ],
  imports: [
    BrowserModule,
    PhotosModule,
    HomeModule,
    AppRoutingModule,
    ErrorsModule
  ],
  
  providers: [{
    provide: HTTP_INTERCEPTORS,
    useClass: RequestInterceptor,
    multi: true
  }],

  bootstrap: [AppComponent]
})
export class AppModule { }
