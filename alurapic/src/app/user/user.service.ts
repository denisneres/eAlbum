import { Injectable } from "@angular/core";
import { TokenService } from "../core/token.service";
import { Subject, Observable, BehaviorSubject } from "rxjs";
import { User } from "./user";
import * as jwt_decode from 'jwt-decode';

@Injectable({providedIn:'root'})
export class UserService {

    private userSubject = new BehaviorSubject<User>(null);
    private userNameLogged: string = ''

    constructor(private tokenService: TokenService){
        this.tokenService.hasToken() && this.decodeAndNotify();
    }

    setToken(token){
        this.tokenService.setToken(token);
        this.decodeAndNotify();
    }

    getUser(): Observable<User> {
        return this.userSubject.asObservable();
    }

    isLogged() {
        return this.tokenService.hasToken();
    }

    private decodeAndNotify() {
        const token = this.tokenService.getToken();
        const user = jwt_decode(token) as User;
        this.userNameLogged = user.name;
        this.userSubject.next(user);
    
    }

    getUserNameLogged():string {
        return this.userNameLogged;
    }

    logout() {
        this.tokenService.removeToken();
        this.userSubject.next(null);
    }

}